provider "azurerm" {
  
  features {}
}

module "resource_group" {
  source      = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/resource-group"
  environment = var.environment
  application = var.application
  app_version = var.app_version
  location    = var.location
}

# Create a billing plan to run the app inside - this could be dedicated to this app, or shared with other apps
module "app_service_plan" {
  source              = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/app-service-plan"
  environment         = var.environment
  application         = var.application
  resource_group_name = module.resource_group.name
  app_version         = var.app_version
  location            = module.resource_group.location

  # using standard-s1 so we can use a slot in the example below
  sku = {
    size     = "S1"
    capacity = 1
    tier     = "Standard"
  }
}

# create a web site 
module "app_service_api" {
  source                        = "git::ssh://git@bitbucket.org/bbcworldwide/terraform-modules.git//azure/modules/app-service-api-with-apim-ip-whitelist?ref=AzureRm2.0"
  environment                   = var.environment
  application                   = var.application
  resource_group_name           = module.resource_group.name
  app_version                   = var.app_version
  location                      = module.resource_group.location
  app_service_plan_id           = module.app_service_plan.id
  api_management_resource_group = var.api_management_resource_group
  api_management_name           = var.api_management_name

  dotnet_version                = "v5.0"
}


module "api_management_api" {
  source                        = "git::ssh://git@bitbucket.org/bbcworldwide/azure-api-management.git//infrastructure/modules/api"
  api_management_resource_group = var.api_management_resource_group
  api_management_name           = var.api_management_name
  api_name                      = "/api-example"
  api_path                      = "api-example"
  # swagger file generated during build
  swagger_json                  = file("ApiExample.json")
  swagger_json_format           = "openapi"
  host                          = module.app_service_api.default_site_hostname
  base_path                     = "/"
}