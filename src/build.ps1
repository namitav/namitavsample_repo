Import-Module -Name $PSScriptRoot\build-functions.psm1 -Force

$config = "Release"

## below select either full framework or dotnet core section and delete the other section ##

#< ######################### .net core version version ######################
  
Import-Module -Name $PSScriptRoot\build-functions.psm1 -Force

$project = "BBC.Greetings.Web"
$packageId = "BBC.Greetings.Web"
$publishedFolder = "$ReleaseDir\$project"

Install-Octo

# Execute the build in stages - Clean > Restore > Compile > Test > Pack > Reporting
Clean -Config $config

# Restore Dependencies
Restore -UseDotNet

# Compile the app
Invoke-BuildCore -Config $config

# Run unit tests - none ATM!
Invoke-TestCore -ProjectNameFilter "*BBC.Greetings.Tests*" -Config $config

# Package up any terraform scripts
$infrastructurePackage = New-ZipPackage -Id "$packageId.Infrastructure" -Version $BuildNumber -FolderToZip "$ReleaseDir\..\infrastructure" -Include "*.tf","*.tfvars" -OutputFolder $ReleaseDir 

# publish web or api project, zip it up and remove the unzipped output
Invoke-PublishCore -Project $project -Config $config

New-Item -ItemType Directory -Force -Path $publishedFolder\files
Get-ChildItem -Path "$publishedFolder\*" -Exclude "deploy.ps1" | Move-Item -Destination "$publishedFolder\files"

$webAppPackage = New-ZipPackage -id "$packageId" -version $BuildNumber -FolderToZip $publishedFolder -outputFolder $ReleaseDir -RemoveFolder

ReportResults

Publish-TCArtifacts $ReleaseDir

# Push the .zip to the octopus server if we have credentials to do so
Publish-ToOctopusGallery $webAppPackage
Publish-ToOctopusGallery $infrastructurePackage

########################## End .net core version ########################>