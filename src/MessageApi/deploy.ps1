
# Get azure app service deployment login from terraform outputs
$terraformOutputStepName = $OctopusParameters["CreateInfrastructure.TeraformStepName"]

$siteCredentialsValue = $OctopusParameters["Octopus.Action[$terraformOutputStepName].Output.TerraformValueOutputs[site_credential]"] 
$siteCredentials = $siteCredentialsValue  | ConvertFrom-Json

$appServiceName = $OctopusParameters["Octopus.Action[$terraformOutputStepName].Output.TerraformValueOutputs[app_service_name]"] 

# Get details of this deployment
$project = $OctopusParameters['Octopus.Project.Name'].Replace(".", "-").Replace(" ", "-")
$environment = ($OctopusParameters['Octopus.Environment.Name'] -Replace "\W.*$","")
$releaseNumber = $OctopusParameters['Octopus.Release.Number']
$dateTime = (Get-Date -Format "yyyyMMddHHmmss")
$proxy = $null
if($OctopusParameters['SecureProxy']){
    $proxy = $OctopusParameters['SecureProxy']
}


# A unique zip name based on the Octopus environment, release, and deployment
$path = $PSScriptRoot
$zipDestination = Join-Path $path "\$project.$environment.$releaseNumber.$dateTime.zip"

# Zip up the current folder ready for deployment
# using exclusion rules. Can use wild cards (*)
$exclude = @("*.ps1","*.zip")
# get files to compress using exclusion filer and zip them
$files = Get-ChildItem -Path $path -Exclude $exclude
Compress-Archive -Path $files -DestinationPath $zipDestination -CompressionLevel Optimal

#Send to Azure using ZipDeploy, See https://docs.microsoft.com/en-us/azure/app-service/app-service-deploy-zip#rest

$apiUrl = "https://$appServiceName.scm.azurewebsites.net/api/zipdeploy"
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $siteCredentials.username, $siteCredentials.password)))
$userAgent = "powershell/1.0"

Write-Host "POSTing $zipDestination to $apiUrl as user $($siteCredentials.username)"

try {
    Invoke-RestMethod -Uri $apiUrl -Headers @{Authorization=("Basic {0}" -f $base64AuthInfo)} -UserAgent $userAgent -Method POST -InFile $zipDestination -ContentType "multipart/form-data" -Proxy $proxy -TimeoutSec 240
    Write-Host "Publish complete"
} catch {
    Write-Host "Publish failed"

    # Note that value__ is not a typo.
    Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ 
    Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription

    Throw $_.Exception
}
