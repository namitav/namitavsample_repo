﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBC.Greetings.Web.Model
{
    public class GreetingResponse
    {
        public string Message { get; set; }
    }
}
