﻿using BBC.Greetings.Web.Model;
using System;


namespace BBC.Greetings.Web.Services
{
   public interface IGreetingService
    {
        GreetingResponse GetGreetingMessage(String name);
    }
}
