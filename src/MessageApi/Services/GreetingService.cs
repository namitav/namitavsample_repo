﻿using BBC.Greetings.Web.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBC.Greetings.Web.Services
{
    public class GreetingService:IGreetingService
    {
        public const string MESSAGE_GENERIC_STRING = "Welcome to BBC Studios ";

        public GreetingResponse GetGreetingMessage(string name)
        {
            return new GreetingResponse() { Message = MESSAGE_GENERIC_STRING + name };
        }   
    }   
}
