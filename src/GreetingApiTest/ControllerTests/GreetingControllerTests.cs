﻿using BBC.Greetings.Web.Controllers;
using BBC.Greetings.Web.Model;
using BBC.Greetings.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBC.Greetings.Test.ControllerTests
{
    [TestFixture]
    public class GreetingControllerTests
    {
        Mock<IGreetingService> mockService;
        public const string MESSAGE_GENERIC_STRING = "Welcome to BBC Studios ";

        [SetUp]
        public void Initialize()
        {
            mockService = new Mock<IGreetingService>();
        }
        [Test]
        public void Get_WhenCalled_ReturnsOkResult()
        {

            var controllerTest = new GreetingController(mockService.Object);


            var result = controllerTest.Get("test");


            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase("  ")]
        public void Get_InvalidValuesPassed_ReturnsBadRequestResult(string value)
        {

            var controllerTest = new GreetingController(mockService.Object);


            var result = controllerTest.Get(value);


            Assert.IsInstanceOf<BadRequestResult>(result.Result);
        }
        [TestCase("Test")]
        [TestCase("1234")]
        public void Get_ValidValuePassed_ReturnsWelcomeMessage(string value)
        {

            var testResult = new GreetingResponse() { Message = MESSAGE_GENERIC_STRING + value };
            mockService.Setup(a => a.GetGreetingMessage(It.IsAny<string>())).Returns(testResult);
            var controllerTest = new GreetingController(mockService.Object);


            var result = controllerTest.Get(value).Result as OkObjectResult;

            Assert.IsInstanceOf<GreetingResponse>(result.Value);
            Assert.AreEqual(testResult.Message, (result.Value as GreetingResponse).Message);
        }
        [Test]
        public void Get_ServiceException_ReturnsNull()
        {

            mockService.Setup(a => a.GetGreetingMessage(It.IsAny<string>())).Throws<Exception>();
            var controllerTest = new GreetingController(mockService.Object);


            var result = controllerTest.Get("Test");


            Assert.IsNull(result.Value);

        }
    }
}
