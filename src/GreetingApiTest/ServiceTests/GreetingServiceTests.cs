﻿using BBC.Greetings.Web.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBC.Greetings.Test.ServiceTests
{
    [TestFixture]
    public class GreetingServiceTests
    {
        GreetingService greetingServiceTest;
        public const string MESSAGE_GENERIC_STRING = "Welcome to BBC Studios ";
        [SetUp]
        public void Initialize()
        {
            greetingServiceTest = new GreetingService();
        }

        [TestCase("Test")]
        [TestCase("1243")]
        public void GetGreeting_WhenCalled_ReturnsGreetingMessage(string value)
        {

            var testResult = MESSAGE_GENERIC_STRING + value;


            var result = greetingServiceTest.GetGreetingMessage(value);


            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result.Message);
            Assert.AreEqual(testResult, result.Message);
        }
    }
}
